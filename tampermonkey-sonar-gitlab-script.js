// ==UserScript==
// @name         SonarQube in Gitlab
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Add sonarqube data in merge request
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js
// @author       Cyrille HEIT
// @match        https://gitlab.com/parcourspatient/*/-/merge_requests/*
// @grant        GM_addStyle
// @grant        GM_xmlhttpRequest
// ==/UserScript==
var projectId;
console.log(location.pathname);

if (location.pathname.startsWith("/parcourspatient/epl-proxy")) {
    projectId = "epl-proxy";
} else if (location.pathname.startsWith("/parcourspatient/epp-etablissement")) {
    projectId = "epl-institution";
} else if (location.pathname.startsWith("/parcourspatient/epp-web-application")) {
    projectId = "epl-carnet";
} else if (location.pathname.startsWith("/parcourspatient/epp-back-office-gestion")) {
    projectId = "epl-back-office-management";
}
var sonarUrl = "https://sonar.forge.enovacom.cloud";
var apiURL = sonarUrl + "/api/measures/component?component=" + projectId + "&metricKeys=coverage%2Cnew_coverage%2Cnew_lines_to_cover%2Cduplicated_lines_density%2Cnew_duplicated_lines_density%2Cnew_lines%2Cnew_code_smells%2Cnew_maintainability_rating%2Cnew_bugs%2Cnew_reliability_rating%2Cnew_vulnerabilities%2Cnew_security_hotspots%2Cnew_security_rating&pullRequest=";
var pullRequest;
function processJSON_Response (response) {
    if (response.status != 200 && response.status != 304) {
        reportAJAX_Error (response);
        return;
    }
    var measures = response.response.component.measures;
    var newCoverage;
    var newCoverageStr;
    if (measures.filter(m => m.metric == "new_coverage").length == 0) {
        newCoverage = 0;
        newCoverageStr = "-";
    } else {
        newCoverage = Number.parseFloat(measures.filter(m => m.metric == "new_coverage")[0].periods[0].value).toFixed(1);
        newCoverageStr = newCoverage + "%";
    }
    var coverage = measures.filter(m => m.metric == "coverage").length > 0 ? Number.parseFloat(measures.filter(m => m.metric == "coverage")[0].value).toFixed(1) : '';
    var newBugs = measures.filter(m => m.metric == "new_bugs").length > 0 ? measures?.filter(m => m.metric == "new_bugs")[0].periods[0].value : '';
    var newVulnerabilities = measures.filter(m => m.metric == "new_vulnerabilities").length > 0 ? measures.filter(m => m.metric == "new_vulnerabilities")[0].periods[0].value : '';
    var newCodeSmells = measures.filter(m => m.metric == "new_code_smells").length > 0 ? measures.filter(m => m.metric == "new_code_smells")[0].periods[0].value : '';
    var newSecurityHotspots = measures.filter(m => m.metric == "new_security_hotspots").length > 0 ? measures.filter(m => m.metric == "new_security_hotspots")[0].periods[0].value : '';
     $('.sonarqube').append(`
<div class="mr-widget-section">
   <div class="mr-widget-info">
      <section class="mr-info-list">
         <table style="border-spacing: 10px;border-collapse: separate;">
         <tr>
            <td align="center"><a href="${sonarUrl}/project/issues?pullRequest=${pullRequest}&id=${projectId}&resolved=false&types=BUG"><strong>${newBugs}</strong></a></td>
            <td>
               <svg style="vertical-align: text-bottom" height="19" version="1.1" viewBox="0 0 16 16" width="19" xml:space="preserve" xmlns:xlink="http://www.w3.org/1999/xlink" style="fill-rule: evenodd; clip-rule: evenodd; stroke-linejoin: round; stroke-miterlimit: 1.41421;">
                  <path d="M8.01 10.9885h1v-5h-1v5zm3-2h1.265l.46.771.775-.543-.733-1.228H11.01v-.316l2-2.343v-2.341h-1v1.972l-1 1.172v-1.144h-.029c-.101-.826-.658-1.52-1.436-1.853l1.472-1.349-.676-.736-1.831 1.678-1.831-1.678-.676.736 1.472 1.349c-.778.333-1.335 1.027-1.436 1.853H6.01v1.144l-1-1.172v-1.972h-1v2.341l2 2.343v.316H4.243l-.733 1.228.775.543.46-.771H6.01v.287l-2 1.912v2.801h1v-2.374l1.003-.959c.018 1.289 1.07 2.333 2.363 2.333h.768c.741 0 1.418-.347 1.767-.907.258-.411.304-.887.16-1.365l.939.898v2.374h1v-2.801l-2-1.912v-.287z" fill-rule="evenodd" style="fill: currentcolor;"></path>
               </svg>
               New Bugs
            </td>
         </tr>
         <tr>
            <td align="center"><a href="${sonarUrl}/project/issues?pullRequest=${pullRequest}&id=${projectId}&resolved=false&types=VULNERABILITY"><strong>${newVulnerabilities}</strong></a></td>
            <td>
               <svg style="vertical-align: text-bottom" height="19" version="1.1" viewBox="0 0 16 16" width="19" xml:space="preserve" xmlns:xlink="http://www.w3.org/1999/xlink" style="fill-rule: evenodd; clip-rule: evenodd; stroke-linejoin: round; stroke-miterlimit: 1.41421;">
                  <path d="M10.8 5H6V3.9a2.28 2.28 0 0 1 2-2.5 2.22 2.22 0 0 1 1.8 1.2.48.48 0 0 0 .7.2.48.48 0 0 0 .2-.7A3 3 0 0 0 8 .4a3.34 3.34 0 0 0-3 3.5v1.2a2.16 2.16 0 0 0-2 2.1v4.4a2.22 2.22 0 0 0 2.2 2.2h5.6a2.22 2.22 0 0 0 2.2-2.2V7.2A2.22 2.22 0 0 0 10.8 5zm-2.2 5.5v1.2H7.4v-1.2a1.66 1.66 0 0 1-1.1-1.6A1.75 1.75 0 0 1 8 7.2a1.71 1.71 0 0 1 .6 3.3z" style="fill: currentcolor;"></path>
               </svg>
               New Vulnerabilities
            </td>
         </tr>
         <tr>
            <td align="center"><a href="${sonarUrl}/project/issues?pullRequest=${pullRequest}&id=${projectId}&resolved=false&types=SECURITY_HOTSPOT"><strong>${newSecurityHotspots}</strong></a></td>
            <td>
               <svg style="vertical-align: text-bottom" height="19" version="1.1" viewBox="0 0 16 16" width="19" xml:space="preserve" xmlns:xlink="http://www.w3.org/1999/xlink" style="fill-rule: evenodd; clip-rule: evenodd; stroke-linejoin: round; stroke-miterlimit: 1.41421;">
                  <g fill="none" fill-rule="evenodd">
                     <path d="M10.2764 2.3205c-.437-.905-1.273-1.521-2.227-1.521-1.402 0-2.549 1.333-2.549 2.959v5.541" stroke="currentColor" stroke-linecap="round" stroke-width="1.14"></path>
                     <path d="M5.2227 5.0215h5.555c1.222 0 2.222 1 2.222 2.222v4.444c0 1.223-1 2.223-2.222 2.223h-5.555c-1.223 0-2.223-1-2.223-2.223v-4.444c0-1.222 1-2.222 2.223-2.222zm2.15279 5.73895h1.25683c.00586-.22266.03663-.4065.09229-.55151.05566-.14502.15527-.28638.29883-.42408l.50537-.47021c.21387-.20801.36914-.41162.46582-.61084.09668-.19922.14502-.42041.14502-.66358 0-.55664-.17944-.98583-.53833-1.2876C9.24243 6.45089 8.73633 6.3 8.083 6.3c-.65626 0-1.16602.16333-1.5293.48999-.36328.32666-.54785.78296-.55371 1.3689h1.48535c.00586-.21973.06299-.39405.17139-.52295.1084-.1289.25049-.19336.42627-.19336.38086 0 .57129.22119.57129.66357 0 .18164-.0564.3479-.1692.49878-.11279.15088-.27758.31714-.49438.49878-.2168.18164-.37353.39624-.47021.6438-.09668.24756-.14502.5852-.14502 1.01294zm-.18018 1.33594c0 .2168.07837.39477.23511.53393.15674.13916.3523.20874.58667.20874.23438 0 .42993-.06958.58667-.20874.15674-.13916.2351-.31714.2351-.53393 0-.2168-.07836-.39478-.2351-.53394-.15674-.13916-.3523-.20874-.58667-.20874-.23438 0-.42993.06958-.58667.20874-.15674.13916-.2351.31714-.2351.53394z" style="fill: currentcolor;"></path>
                  </g>
               </svg>
               New Security Hotspots
            </td>
         </tr>
         <tr>
            <td align="center"><a href="${sonarUrl}/project/issues?pullRequest=${pullRequest}&id=${projectId}&resolved=false&types=CODE_SMELL"><strong>${newCodeSmells}</strong></a></td>
            <td>
               <svg style="vertical-align: text-bottom" height="19" version="1.1" viewBox="0 0 16 16" width="19" xml:space="preserve" xmlns:xlink="http://www.w3.org/1999/xlink" style="fill-rule: evenodd; clip-rule: evenodd; stroke-linejoin: round; stroke-miterlimit: 1.41421;">
                  <path d="M8 2C4.7 2 2 4.7 2 8s2.7 6 6 6 6-2.7 6-6-2.7-6-6-6zm-.5 5.5h.9v.9h-.9v-.9zm-3.8.2c-.1 0-.2-.1-.2-.2 0-.4.1-1.2.6-2S5.3 4.2 5.6 4c.2 0 .3 0 .3.1l1.3 2.3c0 .1 0 .2-.1.2-.1.2-.2.3-.3.5-.1.2-.2.4-.2.5 0 .1-.1.2-.2.2l-2.7-.1zM9.9 12c-.3.2-1.1.5-2 .5-.9 0-1.7-.3-2-.5-.1 0-.1-.2-.1-.3l1.3-2.3c0-.1.1-.1.2-.1.2.1.3.1.5.1s.4 0 .5-.1c.1 0 .2 0 .2.1l1.3 2.3c.2.2.2.3.1.3zm2.5-4.1L9.7 8c-.1 0-.2-.1-.2-.2 0-.2-.1-.4-.2-.5 0-.1-.2-.3-.3-.4-.1 0-.1-.1-.1-.2l1.3-2.3c.1-.1.2-.1.3-.1.3.2 1 .7 1.5 1.5s.6 1.6.6 2c0 0-.1.1-.2.1z" style="fill: currentcolor;"></path>
               </svg>
               New Code Smells
            </td>
         </tr>
         <tr>
            <td align="center"><a href="${sonarUrl}/component_measures?pullRequest=${pullRequest}&id=${projectId}&metric=new_coverage&view=list"><strong>${newCoverageStr}</strong></a></td>
            <td>
               <svg style="vertical-align: text-bottom" width="19" height="19" viewBox="0 0 40 40" class="donut">
                  <circle class="donut-hole" cx="20" cy="20" r="15.91549430918954" fill="#fff"></circle>
                  <circle class="donut-ring" cx="20" cy="20" r="15.91549430918954" fill="transparent" stroke-width="6", stroke="rgb(212, 51, 63)"></circle>
                  <circle class="donut-segment" cx="20" cy="20" r="15.91549430918954" fill="transparent" stroke-width="6" stroke-dasharray="${newCoverage},${100 - newCoverage}" stroke-dashoffset="25" stroke="rgb(0, 170, 0)"></circle>
               </svg>
               Coverage
            </td>
         </tr>
         <tr>
            <td><strong align="center">${coverage}%</strong></td>
            <td>
               <svg style="vertical-align: text-bottom" width="19" height="19" viewBox="0 0 40 40" class="donut">
                  <circle class="donut-hole" cx="20" cy="20" r="15.91549430918954" fill="#fff"></circle>
                  <circle class="donut-ring" cx="20" cy="20" r="15.91549430918954" fill="transparent" stroke-width="6", stroke="rgb(212, 51, 63)"></circle>
                  <circle class="donut-segment" cx="20" cy="20" r="15.91549430918954" fill="transparent" stroke-width="6" stroke-dasharray="${coverage},${100 - coverage}" stroke-dashoffset="25" stroke="rgb(0, 170, 0)"></circle>
               </svg>
               Coverage estimated after merge
            </td>
         </tr>
      </section>
   </div>
</div>
        `);
}
function reportAJAX_Error (response) {
    console.error (`Appel sonarqube => Error ${response.status}!  ${response.statusText}`);
    if (response.status == 401) {
       $('.sonarqube').append(`
<div class="mr-widget-section sonarqube-login-form">
   <div class="mr-widget-info">
      <section class="mr-info-list" style="padding-bottom: 8px">
         You need to sign in to the Sonarqube platform first in order to have the indicators.
      </section>
      <section class="mr-info-list" style="padding-bottom: 16px">
         <div class="d-flex">
            <input type="text" name="username" placeholder="Username" id="username" class="form-control" title="This field is required." required="required" style="margin-right: 8px">
            <input type="password" name="password" placeholder="Password" id="password" class="form-control bottom" title="This field is required." required="required" style="margin-right: 8px">
            <input type="submit" name="login_sonarqube" value="Sign in" class="btn-success btn btn-login-sonarqube">
         </div>
      </section>
   </div>
</div>
      `);
        $('.btn-login-sonarqube').on('click', function() {
            var formData = new FormData();
            formData.append("login", $('#username').val());
            formData.append("password", $('#password').val());
            GM_xmlhttpRequest ( {
                method: "POST",
                url: sonarUrl + "/api/authentication/login",
                responseType: "json",
                onload: processJSON_loginOk,
                onabort: processJSON_loginError,
                onerror: processJSON_loginError,
                ontimeout: processJSON_loginError,
                data: formData
            } );
        });
    }
}
function processJSON_loginOk (response) {
    if (response.status != 200 && response.status != 304) {
        processJSON_loginError(response);
        return;
    }
    console.log("Login Ok");
    $(".sonarqube-login-form").remove();
    getIndicators();
}
function processJSON_loginError (response) {
    console.error(`Appel Login sonarqube => Error ${response.status}!  ${response.statusText}`);
    $(".sonarqube-login-form").prepend(`
<div class="flash-container flash-container-page sticky">
   <div class="flash-alert mb-2">
      <span>Could not authenticate you to SonarQube platform".</span>
   </div>
</div>
`);
}
function getIndicators() {

    GM_xmlhttpRequest ( {
            method:         "GET",
            url:            apiURL + pullRequest,
            responseType:   "json",
            onload:         processJSON_Response,
            onabort:        reportAJAX_Error,
            onerror:        reportAJAX_Error,
            ontimeout:      reportAJAX_Error
        } );
}
(function() {
    'use strict';
    var interval = setInterval(function() {
        if ($('.mr-state-widget').length && $('.label-branch').length) {
            var data = $('*[data-testid="breadcrumb-current-link"]').contents(":not(:empty)").first().text();
            pullRequest = data.replace('!', '');
            $('.mr-state-widget').append(`
<div class="mr-section-container mr-widget-workflow sonarqube">
   <div class="mr-widget-section ">
      <div class="mr-widget-body ">
         <div class="media">
            <div class="d-flex align-self-start align-self-center" size="24">
               <div class="square s24 h-auto d-flex-center append-right-default">
                  <img  height="24" width="24" src="${sonarUrl}/favicon.ico"/>
               </div>
            </div>
            <div class="media-body d-flex flex-align-self-center align-items-center">
               <div class="js-code-text code-text">
                  <div>
                     <a href="${sonarUrl}/dashboard?pullRequest=${pullRequest}&id=${projectId}">Sonarqube
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
        `);
            getIndicators();
            clearInterval(interval);
        }
        
    }, 100);
})();
